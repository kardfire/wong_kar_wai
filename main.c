/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 20:27:55 by akpenou           #+#    #+#             */
/*   Updated: 2016/01/30 22:53:41 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ncurses.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "rush.h"

# define MINL	15
# define MINC	30

# define XPART (x % (xmax / size))
# define YPART (y % (ymax / size))

void	ft_printtab(int *tab, int size)
{
	int	x;
	int	y;
	int	i = 0;
	int	xmax;
	int	ymax;

	y = -1;
	xmax = ((COLS - 1) / size) * size;
	ymax = ((LINES - 1) / size) * size;
	while (++y <= ymax)
	{
		x = -1;
		while (++x <= xmax)
		{
			if (!XPART && !YPART)
				mvaddch(y, x, '+');
			else if (!XPART && YPART)
				mvaddch(y, x, '|');
			else if (XPART && !YPART)
				mvaddch(y, x, '-');
			else if (YPART == ymax / (2 * size) &&
					XPART == (xmax / size - ft_nbrlen(tab[i])) / 2 + 1)
			{
				if (tab[i])
					mvprintw(y, x, "%d", tab[i]);
				x += 3;
				++i;
			}
		}
	}
	refresh();
}

#define IS_ARROW (ch == KEY_LEFT || ch == KEY_RIGHT || ch == KEY_UP || ch == KEY_DOWN)

void	ft_playkey(int **tab, int size)
{
	int	ch;

	while ((ch = getch()) != 27)
	{
		if (ch == KEY_RESIZE && !erase())
		{
			refresh();
			if (LINES < MINL || COLS < MINC)
				printw("This screen is too small");
			else
				ft_printtab(*tab, size);
			refresh();
		}
		else if (IS_ARROW && !erase())
			ft_input(tab, ch, size);
		else if ((ch == 'p' || ch == ' ') && !erase())
			ft_printtab(*tab, size);
		else if (ch == 't' && !erase())
		{
			printw("%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d\n%d\t%d\t%d\t%d\n",
			tab[0][0],tab[0][1],tab[0][2],tab[0][3],tab[0][4],tab[0][5],tab[0][6],tab[0][7],tab[0][8],
			tab[0][9],tab[0][10],tab[0][11],tab[0][12],tab[0][13],tab[0][14],tab[0][15],tab[0][16]);
			refresh();
		}
		else if (ch =='q' || ch == 27)
			break ;
		refresh();
//		ft_error_moves(*tab);
	}
}


int main(void)
{
	int		*tab;
	int		size;

	size = 4;
	ft_init_grid(&tab);
	initscr();
	raw();
	noecho();
	refresh();
	keypad(stdscr, TRUE);
	ft_error_checking();
	srand(time(NULL));
	printw("LET'S PLAY THE GAME\nPress space or P");
	ft_playkey(&tab, size);
	endwin();
	return (0);
}
