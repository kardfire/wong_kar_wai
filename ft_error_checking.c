/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_checking.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 18:41:56 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 21:10:01 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

static int	ft_is_powtwo()
{
	int		x;

	x = WIN_VALUE;
	while (((x % 2) == 0) && x > 1)
		x /= 2;
	if (x == 1)
		return (1);
	return (-1);
}

int			ft_error_checking()
{
	if (ft_is_powtwo() == -1)
	{
		erase();
		printw("/!\\ Invalid WIN_VALUE, must be a power of 2 /!\\\n");
		printw("- Press any key to close the program -");
		getch();
		endwin();
		exit(EXIT_FAILURE);
	}
	return (0);
}
