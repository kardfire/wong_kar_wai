/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_input.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 17:10:26 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 21:46:33 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

int		ft_input(int **grid, int input, int size)
{
	int		move;

	move = 0;
	if (input == KEY_UP || input == KEY_DOWN || input == KEY_RIGHT
			|| input == KEY_LEFT)
	{
		if (input == KEY_UP || input == KEY_DOWN)
			move = ft_move_du(input, grid);
		else if (input == KEY_RIGHT || input == KEY_LEFT)
			move = ft_move_lr(input, grid);
		if (move == 0)
		{
			ft_error_moves(*grid);
			ft_printtab(*grid, size);
		}
		else
		{
			ft_add_case(grid);
			ft_printtab(*grid, size);
		}
	}
	return (0);
}
