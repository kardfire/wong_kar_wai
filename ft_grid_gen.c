/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_grid_gen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 09:30:57 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 21:57:37 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

int		ft_is_empty(int *grid)
{
	int		count;

	count = 0;
	while (count < 16)
	{
		if (grid[count] != 0)
			return (1);
		count++;
	}
	return (2);
}

int		ft_return_win_lose(int *grid)
{
	int		c;
	
	c = 0;
	while (c < 16)
	{
		if (grid[c] == WIN_VALUE)
		{
			printw("\nCongratz Dude, You Won ¯\\_(o.O)_/¯\nPress \"return\" to keep going, or \"escape\" to stop playing");
			return (2048);
		}
		c++;
	}
	return (0);
}

void	ft_add_case(int **grid)
{
	int		ret;
	int		value;
	int		position;

	ret = ft_is_empty(*grid);
	position = 0;
	while (ret != 0)
	{
		while ((*grid)[position] != 0)
		{
			position = rand() % 17;
		}
		value = rand() % 17 > 8 ? 4 : 2;
		(*grid)[position] = value;
		ret--;
	}
}

void	ft_init_grid(int **grid)
{
	int		count;

	count = 0;
	(*grid) = (int*)malloc(sizeof(int) * 16);
	while (count < 16)
	{
		(*grid)[count] = 0;
		count++;
	}
	ft_add_case(grid);
}
