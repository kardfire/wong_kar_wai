/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_moves.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 19:57:09 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 21:46:39 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

static int		ft_is_full(int *grid)
{
	int		c;

	c = 0;
	while (c < 16)
	{
		if (grid[c] == 0)
			return (0);
		++c;
	}
	return (1);
}

static void		ft_copy_tab(int *grid, int **copy)
{
	int		c;

	c = 0;
	(*copy) = (int*)malloc(sizeof(int) * 16);
	while (c < 16)
	{
		(*copy)[c] = grid[c];
		++c;
	}
}

void			ft_error_moves(int *grid)
{
	int		*copy;
	int		move;

	move = 0;
	if (ft_is_full(grid) == 1)
	{
		ft_copy_tab(grid, &copy);
		move += ft_move_du(KEY_UP, &copy);
		move += ft_move_du(KEY_DOWN, &copy);
		move += ft_move_lr(KEY_RIGHT, &copy);
		move += ft_move_lr(KEY_LEFT, &copy);
		if (move == 0)
		{
			erase();
			printw("You Lost\n- Press any key to quit -");
			getch();
			endwin();
			exit(EXIT_FAILURE);
		}
	}
}
