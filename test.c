/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 09:24:40 by akpenou           #+#    #+#             */
/*   Updated: 2016/01/30 16:45:42 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#define LINES 20
#define COLS 20


int main()
{
	int	x;
	int	y;
	int	p;
	int	maxc;
	int	nbc;
	
	y = -1;
	if ((nbc = ((COLS < LINES ? COLS : LINES) - 5) / 4) % 2 == 0)
		--nbc;
	printf("LINE = %d \t COLS = %d\n", LINES, COLS);
	maxc = 5 + 4 * nbc;
	printf("nbc = %d \t maxc = %d", nbc, maxc);
}
