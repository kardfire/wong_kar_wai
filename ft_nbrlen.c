/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akpenou <akpenou@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/16 13:51:47 by akpenou           #+#    #+#             */
/*   Updated: 2016/01/25 12:08:49 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

int	ft_nbrlen(unsigned int n)
{
	unsigned int	pow;
	int		len;

	len = 1;
	pow = 1;
	while (n / pow > 10 - 1 && ++len)
		pow *= 10;
	return (len);
}
