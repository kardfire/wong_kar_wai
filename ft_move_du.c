/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_du.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 11:37:52 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 21:09:58 by akpenou          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"

static int		ft_sup_void_down(int **grid);
static int		ft_sup_void_up(int **grid);
static int		ft_move_down(int **grid);
static int		ft_move_up(int **grid);

int		ft_move_du(int keyconst, int **grid)
{
	int		move;

	move = 0;
	if (keyconst == KEY_UP)
		move = ft_move_up(grid);
	if (keyconst == KEY_DOWN)
		move = ft_move_down(grid);
	return (move);
}

static int		ft_sup_void_up(int **grid)
{
	int		move;
	int		count;
	int		count2;

	move = 0;
	count = 0;
	while (count < 12)
	{
		count2 = count + 4;
		if ((*grid)[count] == 0)
		{
			while ((*grid)[count2] && (*grid)[count2] == 0)
				count2 += 4;
			if ((*grid)[count2])
			{
				(*grid)[count] = (*grid)[count2];
				(*grid)[count2] = 0;
				move++;
			}
		}
		count++;
	}
	return (move);
}

static int		ft_move_up(int **grid)
{
	int		move;
	int		count;

	count = 0;
	move = 0;
	while (count < 12)
	{
		move += ft_sup_void_up(grid);
		if ((*grid)[count + 4])
		{
			if ((*grid)[count] == (*grid)[count + 4])
			{
				(*grid)[count] += (*grid)[count + 4];
				(*grid)[count + 4] = 0;
				move++;
			}
		}
		count++;
	}
	return (move);
}

static int		ft_sup_void_down(int **grid)
{
	int		move;
	int		count;
	int		count2;

	move = 0;
	count = 15;
	while (count > 3)
	{
		count2 = count - 4;
		if ((*grid)[count] == 0)
		{
			while ((*grid)[count2] && (*grid)[count2] == 0)
				count2 -= 4;
			if ((*grid)[count2])
			{
				(*grid)[count] = (*grid)[count2];
				(*grid)[count2] = 0;
				move++;
			}
		}
		count--;
	}
	return (move);
}

static int		ft_move_down(int **grid)
{
	int		move;
	int		count;

	count = 15;
	move = 0;
	while (count > 3)
	{
		move += ft_sup_void_down(grid);
		if ((*grid)[count - 4])
		{
			if ((*grid)[count] == (*grid)[count - 4])
			{
				(*grid)[count] += (*grid)[count - 4];
				(*grid)[count - 4] = 0;
				move++;
			}
		}
		count--;
	}
	return (move);
}
