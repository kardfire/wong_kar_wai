/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_lr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 11:56:45 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 18:22:39 by fsacrepe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush.h"
static int		ft_sup_void_right(int **grid);
static int		ft_sup_void_left(int **grid);
static int		ft_move_right(int **grid);
static int		ft_move_left(int **grid);

int		ft_move_lr(int keyconst, int **grid)
{
	int		move;

	move = 0;
	if (keyconst == KEY_RIGHT)
		move = ft_move_right(grid);
	if (keyconst == KEY_LEFT)
		move = ft_move_left(grid);
	return (move);
}

static int		ft_sup_void_right(int **grid)
{
	int		move;
	int		count;
	int		count2;

	count = 3;
	move = 0;
	while (count != 0)
	{
		count2 = count - 1;
		if ((*grid)[count] == 0)
		{
			while (count2 != 0 && count2 != 4 && count2 != 8 && count2 != 12
					&& (*grid)[count2] == 0)
				count2--;
			if ((*grid)[count2])
			{
				(*grid)[count] = (*grid)[count2];
				(*grid)[count2] = 0;
				move++;
			}
		}
		if (count == 15 || count == 14 || count == 13)
			count -= 13;
		else
			count += 4;
	}
	return (move);
}

static int		ft_move_right(int **grid)
{
	int		move;
	int		count;

	move = 0;
	count = 3;
	while (count != 0)
	{
		move += ft_sup_void_right(grid);
		if ((*grid)[count - 1])
			if ((*grid)[count] == (*grid)[count - 1])
			{
				(*grid)[count] += (*grid)[count - 1];
				(*grid)[count - 1] = 0;
				move++;
			}
		if (count == 15 || count == 14 || count == 13)
			count -= 13;
		else
			count += 4;
	}
	return (move);
}

static int		ft_sup_void_left(int **grid)
{
	int		move;
	int		count;
	int		count2;

	move = 0;
	count = 0;
	while (count != 3)
	{
		count2 = count + 1;
		if ((*grid)[count] == 0)
		{
			while (count2 != 3 && count2 != 7 && count2 != 11 
					&& count2 != 15 && (*grid)[count2] == 0)
				count2++;
			if ((*grid)[count2])
			{
				(*grid)[count] = (*grid)[count2];
				(*grid)[count2] = 0;
				move++;
			}
		}
		if (count == 12 || count == 13 || count == 14)
			count -= 11;
		else
			count += 4;
	}
	return (move);
}

static int		ft_move_left(int **grid)
{
	int		move;
	int		count;

	move = 0;
	count = 0;
	while (count != 3)
	{
		move += ft_sup_void_left(grid);
		if ((*grid)[count + 1])
			if ((*grid)[count] == (*grid)[count + 1])
			{
				(*grid)[count] += (*grid)[count + 1];
				(*grid)[count + 1] = 0;
				move++;
			}
		if (count == 12 || count == 13 || count == 14)
			count -= 11;
		else
			count += 4;
	}
	return (move);
}
