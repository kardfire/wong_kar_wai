/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fsacrepe <fsacrepe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 11:48:04 by fsacrepe          #+#    #+#             */
/*   Updated: 2016/01/30 20:05:20 by fsacrepe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RUSH_H
# define RUSH_H

# include <ncurses.h>
# include <curses.h>
# include <stdlib.h>
# include <unistd.h>
# include <time.h>

enum	e_const
{
	WIN_VALUE = 64
};


void	ft_printtab(int *tab, int size);
void	ft_playkey(int **tab, int size);
int					ft_nbrlen(unsigned int n);
int					ft_error_checking(void);
int					ft_input(int **grid, int input, int size);
int					ft_move_du(int keyconst, int **grid);
int					ft_move_lr(int keyconst, int **grid);
int					ft_is_empty(int *grid);
void				ft_add_case(int **grid);
void				ft_init_grid(int **grid);
void				ft_print_grid(int *grid);
void				ft_error_moves(int *grid);
//static int			ft_move_down(int **grid);
//static int			ft_move_up(int **grid);
//static int			ft_move_left(int **grid);
//static int			ft_move_right(int **grid);
//static int			ft_sup_void_down(int **grid);
//static int			ft_sup_void_up(int **grid);
//static int			ft_sup_void_left(int **grid);
//static int			ft_sup_void_right(int **grid);
//static void			ft_copy_tab(int *grid, int **copy);

#endif
